import UIKit
import SwiftMessages
import ANLoader
import Alamofire
import SwiftyJSON

@available(iOS 13.0, *)
class DataViewModel: NSObject {

// MARK: - For Binding ViewModel-ViewController

private(set) var jsonListData: DataList! {
    didSet {
    self.bindViewModelToController()
}}

var bindViewModelToController : (() -> Void) = {}

override init() {
    super.init()
    // Calling metgod to get data from url.
    callFuncToGetData()
}

// MARK: - Getting Data From Url

private func callFuncToGetData() {
    if isRefreshing == false {
    ANLoader.showLoading()
    }
    WebService().postRequest(methodName: StringLiterals.jsonUrl,
    parameter: [:]) { [weak self] (response) in
            ANLoader.hide()
               if let newResponse = response as? String {
               let jsonData = Data((newResponse).utf8)
               let jsonDecoder = JSONDecoder()

                do {
                var data = try jsonDecoder.decode(DataList.self, from: jsonData)
                for (index, item) in data.rows.enumerated() {
                if item.title == nil && item.imageHref == nil && item.description == nil {
                    data.rows.remove(at: index)
                    }
                }
                  self?.jsonListData = data
                } catch {
                }
          }
        }
    }
}
