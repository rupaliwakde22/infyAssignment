import Foundation

struct JsonListData: Codable {

// MARK: - mapping the data fetched from rows

var title: String?
var description: String?
var imageHref: String?

// MARK: - coding keys enumeration

enum CodingKeys: String, CodingKey {
       case title
       case description
       case imageHref
}

// MARK: - decoding values

init(from decoder: Decoder) throws {
      let values = try decoder.container(keyedBy: CodingKeys.self)
      title = try values.decode(String?.self, forKey: .title)
      description = try values.decode(String?.self, forKey: .description)
      imageHref = try values.decode(String?.self, forKey: .imageHref)
 }
}

// MARK: - Modelling Struct

struct DataList: Codable {
      var rows: [JsonListData]
      var title: String = ""
}

// MARK: - String Extension

extension String {
   func isValidTitle() -> Bool {
    return DataViewModel().jsonListData.title.count > 1
   }
}
