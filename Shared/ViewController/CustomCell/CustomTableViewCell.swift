import UIKit

class CustomTableViewCell: UITableViewCell {

// MARK: PRIVATE LET DECLARATION:

private let labTitle = UILabel()
private let labDescription = UILabel()
private let imgView = UIImageView()
private let labSeperator = UILabel()
private let rightInset: CGFloat = -10.0
private let leftInset: CGFloat = 10.0

override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
 super.init(style: style, reuseIdentifier: reuseIdentifier)

   // Calling UI Element setting
   setBgColor()
   addingElemetsOnContentVw()
   addingElemetsProperties()

   // Calling Setting Constraints Method
   setTitleConstraints()
   setImageConstraints()
   setDescriptionConstraints()
   setSeperatorConstraints()
}

required init?(coder aDecoder: NSCoder) {
  fatalError("init(coder:) has not been implemented")
}
override func awakeFromNib() {
        super.awakeFromNib()
}

// MARK: - For Initialising UI Of Cell

func initialiseOutlet(json1: JsonListData) {
            labTitle.text = json1.title
            labDescription.text = json1.description
            // guarding empty image
    guard let img = json1.imageHref,
          let url = URL(string: "\(img)")  else {
            imgView.image = UIImage.init(named: "no_img")
            return
            }
            imgView.af_setImage(withURL: url)
            imgView.isHidden = false
    }
}

private extension CustomTableViewCell {

// MARK: - Setting Background Theme

func setBgColor() {
       self.backgroundColor = .clear
       contentView.backgroundColor = .clear
}

// MARK: - Adding Elements On Content

func addingElemetsOnContentVw() {
        self.contentView.addSubview(labTitle)
        self.contentView.addSubview(imgView)
        self.contentView.addSubview(labDescription)
        self.contentView.addSubview(labSeperator)
}

// MARK: - Adding Properties On Elements

func addingElemetsProperties() {
        labTitle.textColor = .white
        // 0 number of lines for infinity max count.
        labTitle.numberOfLines = 0
        labTitle.textAlignment = .center
        labDescription.textColor = .gray
        labDescription.numberOfLines = 0
        labDescription.textAlignment = .left
        labSeperator.backgroundColor = .white
        // scale aspect fit mode for image
        imgView.contentMode = .scaleAspectFit
}

// MARK: - Setting Title Constraints

func setTitleConstraints() {
        labTitle.mas.makeConstraints(closure: { make in
              make.top.equalTo()(NSNumber(value: 40))
              make.left.equalTo()(leftInset)
              make.right.equalTo()(rightInset)
    })
}

// MARK: - Setting Description Constraints

func setDescriptionConstraints() {
        labDescription.mas.makeConstraints(closure: { make in
            make.top.equalTo()(NSNumber(value: 200))
            make.left.equalTo()(leftInset)
            make.right.equalTo()(rightInset)
            // for reaching bottom at 40 with flexible content,to make cell dynamic height.
            make.bottom.equalTo()(NSNumber(value: -5))
    })
}

// MARK: - Setting Image Constraints

func setImageConstraints() {
        imgView.mas.makeConstraints(closure: { make in
            make.top.equalTo()(NSNumber(value: 80))
            make.left.equalTo()(leftInset)
            make.right.equalTo()(rightInset)
            make.height.equalTo()(NSNumber(value: 100))
    })
}

// MARK: - Setting Seperator Constraints

func setSeperatorConstraints() {
        labSeperator.mas.makeConstraints(closure: {  make in
            make.bottom.equalTo()(NSNumber(value: 8))
            make.height.equalTo()(NSNumber(value: 1))
            make.left.equalTo()(leftInset)
            make.right.equalTo()(rightInset)
        })
   }
}
