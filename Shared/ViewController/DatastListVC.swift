import UIKit
import SwiftyMasonry
import AlamofireImage
import SwiftMessages
import ANLoader

@available(iOS 13.0, *)
class DatastListVC: UIViewController {

// MARK: - Private Lets

private var dataViewModelObject = DataViewModel()
private var dataArr = [JsonListData]()
private let dataTable = UITableView()
private let refreshControl = UIRefreshControl()
private let cellReuseIdentifier = "cell"

// MARK: - View Controller Life Cycle

override func viewDidLoad() {
    super.viewDidLoad()
    // Setting Background color
    self.view.backgroundColor = .white
    // Calling Methods
    callToViewCategoryModelForUIUpdate()
    setNavBar()
    setRefreshController()
 }
}

private extension DatastListVC {

// MARK: - Setting Pull To Refresh

func setRefreshController() {
     refreshControl.tintColor = .white
     refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
     dataTable.addSubview(refreshControl)
}

// MARK: - Refresh Action Method

@objc func refresh(_ sender: AnyObject) {
      isRefreshing = true
      callToViewCategoryModelForUIUpdate()
}

// MARK: - Setting Navigation Bar

func setNavBar() {
      navigationController?.navigationBar.barTintColor = StringLiterals.Colors.ColorNavBar
      navigationController?.navigationBar.isTranslucent = false
      // Calling ShowUI function
      showUI()
}

// MARK: - Showing UI

func showUI() {
       dataTable.backgroundColor = StringLiterals.Colors.ColorBg
       dataTable.clipsToBounds = true
       dataTable.separatorStyle = .none
       dataTable.register(CustomTableViewCell.self, forCellReuseIdentifier: "cell")
       self.view.addSubview(dataTable)
        // Setting Constraints for TableView
       dataTable.mas.makeConstraints(closure: { make in
            make.top.equalTo()(view)
            make.left.equalTo()(view)
            make.right.equalTo()(view)
            make.bottom.equalTo()(view)
        })
}

// MARK: - Updating UI

func callToViewCategoryModelForUIUpdate() {
       // Binding of view with model
       self.dataViewModelObject.bindViewModelToController = {
       self.updateDataSource()
      }
}

// MARK: - Updating Data Table From Url

func updateDataSource() {
    dataArr = self.dataViewModelObject.jsonListData.rows
    guard dataArr.count != 0
    else {
          StringLiterals.SwiftMsg.status.configureContent(body: ("Data Not Found"))
          SwiftMessages.show(config: StringLiterals.SwiftMsg.statusConfig, view: StringLiterals.SwiftMsg.status)
          return
            }

    self.title = self.dataViewModelObject.jsonListData.title
    reloadTable()
}

// MARK: - Reloading Table

func reloadTable() {
        dataTable.delegate = self
        dataTable.dataSource = self
        dataTable.reloadData()
        refreshControl.endRefreshing()
  }
}

// MARK: - Table View DataSource Methods

extension DatastListVC: UITableViewDelegate, UITableViewDataSource {

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
}
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell =
        tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier)
        as? CustomTableViewCell
          else {
            fatalError()
            }
        cell.selectionStyle = .none
        cell.initialiseOutlet(json1: dataArr[indexPath.row] )
        return cell
    }
}
