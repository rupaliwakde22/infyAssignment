import XCTest
@testable import infyAssignment

class DataModelTests: XCTestCase {

    func testDataModel_ShouldPassIfValidateTitle() {
        let dataModel = DataViewModel().jsonListData.title
       XCTAssertTrue(dataModel.isValidTitle())
    }
}
