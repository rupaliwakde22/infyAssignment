import XCTest
@testable import infyAssignment

class TableViewValidationTests: XCTestCase {

    func testEmptyTableRow() {
        let table = UITableView()
        XCTAssertEqual(table.rowHeight, 0, "Row height was not zero.")
    }

}
