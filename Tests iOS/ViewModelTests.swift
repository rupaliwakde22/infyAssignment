import XCTest
@testable import infyAssignment

class ViewModelTests: XCTestCase {

    func testJSONMapping() throws {
        WebService().postRequest(methodName: StringLiterals.jsonUrl, parameter: [:]) { (response) in
        guard let newResponse = response as? String
      else {
            XCTFail("Failing Response : Incorrect format")
            return
           }
            let jsonData = Data((newResponse).utf8)
            let jsonDecoder = JSONDecoder()
            do {
                let data = try jsonDecoder.decode(DataList.self, from: jsonData)
            XCTAssertEqual(data.title, "Flag")
            } catch {
            }
        }
    }
}
